module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      width: {
        "main": "calc(100% - 5rem)"
      },
      backgroundImage: theme => ({
        "dummy-report": "url('./report.jpg')"
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
