import { BrowserRouter as Router, Switch, Route,  Redirect } from "react-router-dom";
import AsideComponent from './components/AsideComponent'
import DashboardComponent from './components/DashboardComponent'
import CoursesComponent from './components/CoursesComponent'
import NotFoundComponent from './components/NotFoundComponent'


function App() {
  return (
    <Router>
      <AsideComponent />
      <div className="fixed left-20 top-0 w-main h-screen overflow-y-auto">
        <Switch>
          <Route exact path="/">
            <Redirect to="/dashboard" />
          </Route>
          <Route path="/dashboard">
            <DashboardComponent />
          </Route>
          <Route path="/courses">
            <CoursesComponent />
          </Route>
          <Route path="*">
            <NotFoundComponent />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
