export default function DashboardComponent() {
    return (
        <div className="flex flex-col w-full">
            <span className="text-2xl m-20 font-medium">Dashboard</span>
            <div className="flex flex-col mt-10 px-20">
                <ul className="flex flex-row mx-auto">
                    <li className="flex flex-col mx-8">
                        <div className="border text-center py-6 px-28 text-xl font-medium">12</div>
                        <span className="mt-4 mx-auto">Courses</span>
                    </li>
                    <li className="flex flex-col mx-8">
                        <div className="border text-center py-6 px-28 text-xl font-medium">3</div>
                        <span className="mt-4 mx-auto">Completed</span>
                    </li>
                    <li className="flex flex-col mx-8">
                        <div className="border text-center py-6 px-28 text-xl font-medium">6</div>
                        <span className="mt-4 mx-auto">Wishlist</span>
                    </li>
                    <li className="flex flex-col mx-8">
                        <div className="border text-center py-6 px-28 text-xl font-medium">9</div>
                        <span className="mt-4 mx-auto">Archived</span>
                    </li>
                </ul>

                <div className="flex flex-col mt-20 mx-auto relative bg-dummy-report bg-opacity-10">
                    <img src="/report.jpg" className="opacity-0" />
                </div>
            </div>
        </div>
    )
}
