export default function NotFoundComponent() {
    return (
        <div>
            <div>
                <span>404</span>
            </div>
            <div>
                <span>This page could not be found.</span>
            </div>
        </div>
    )
}