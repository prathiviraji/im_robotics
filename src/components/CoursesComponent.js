export default function CoursesComponent() {

    const add3Dots = (string) => {
        const limit = 90;
        const dots = "...";
        if(string.length > limit) {
            string = string.substring(0,limit) + dots;
        }
        return string;
    }

    return (
        <div className="flex flex-col w-full">
            <span className="text-2xl ml-20 mt-20 font-medium">Courses</span>
            <div className="flex flex-col mt-10 px-20">
                <ul className="flex flex-wrap">
                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>

                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>

                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>

                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>

                    <li className="w-80 border mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>
                    
                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>

                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>

                    <li className="w-80 border mr-4 mb-4">
                        <div className="w-full h-80 bg-gray-500">

                        </div>
                        <div className="flex flex-col m-4">
                            <span className="font-medium">Course 1</span>
                            <span className="mt-2 text-sm text-gray-500">{add3Dots("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.")}</span>
                            <span className="font-medium mt-2">Free - <span className="text-gray-400 line-through">$99</span></span>
                        </div>
                    </li>
                    
                    


                </ul>
            </div>
        </div>
    )
}
