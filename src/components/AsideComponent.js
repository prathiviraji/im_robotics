import { useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';


function AsideComponent() {
    let history = useHistory();
    let data = {
        "/dashboard": ".dashboard",
        "/courses": ".courses"
    }
    history.listen((route) => {
        const className = data[route.pathname];
        if(className){
            const elems = document.querySelectorAll(".aside .active-menu");
            for(var i=0;i<elems.length;i++){
                elems[i].classList.remove("active-menu");
            }
            document.querySelector(".aside "+className).classList.add("active-menu")
        }
    })

    return (
        <div className="fixed left-0 top-0 w-20 h-screen border-r flex flex-col aside">
            <div className="flex flex-col">
                <span className="text-2xl font-semibold mx-auto mt-4">IM</span>
            </div>
            <ul className="flex flex-col w-full mt-auto mb-8">
                <li>
                    <Link to="/dashboard" className="dashboard flex flex-col text-gray-400 hover:text-gray-800 font-medium active-menu cursor-pointer">
                        <span className="material-icons mx-auto">dashboard</span>
                        <span className="text-xs mx-auto mt-2">Dashboard</span>
                    </Link>
                </li>
                <li className="mt-8">
                    <Link to="/courses" className="courses flex flex-col text-gray-400 hover:text-gray-800 font-medium cursor-pointer">
                        <span className="material-icons mx-auto">subject</span>
                        <span className="text-xs mx-auto mt-2">Courses</span>
                    </Link>
                </li>
            </ul>

            <ul className="flex flex-col mb-auto mt-8">
                <li className="flex flex-col text-gray-400 hover:text-gray-800 cursor-pointer font-medium ">
                    <span className="material-icons mx-auto">search</span>
                    <span className="text-xs mx-auto mt-2">Search</span>
                </li>
                <li className="flex flex-col mt-8 text-gray-400 hover:text-gray-800 cursor-pointer font-medium ">
                    <span className="material-icons mx-auto">notifications</span>
                    <span className="text-xs mx-auto mt-2">Notice</span>
                </li>
                <li className="flex flex-col mt-8 text-gray-400 hover:text-gray-800 cursor-pointer font-medium ">
                    <span className="material-icons mx-auto">settings</span>
                    <span className="text-xs mx-auto mt-2">Settings</span>
                </li>
            </ul>

            <div className="flex flex-col">
                <div className="w-10 h-10 bg-black rounded-full mx-auto mb-4">
                    
                </div>
            </div>
        </div>
    )
}

export default AsideComponent;